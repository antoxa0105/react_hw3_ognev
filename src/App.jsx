import './Reset.css';
import './App.scss'
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ItemList from './components/ItemList/ItemList';
import Cart from './components/Cart/Cart';
import ListItemContainer from './components/ListItemContainer/ListItemContainer';
import Favorites from './components/Favorites/Favorites';



const App = () => {

  return (
    <BrowserRouter>
    <ListItemContainer>
      <Routes>
        <Route path="/" element={
        <ItemList />
        } /> 
        <Route path="favorites" element={
        <Favorites />
        } /> 
        <Route path="cart" element={
        <Cart />
        } /> 
      </Routes>
      </ListItemContainer>
    </BrowserRouter>
  );
};

export default App;

