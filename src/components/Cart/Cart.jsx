import React, {useContext, useEffect} from 'react';
import Header from '../Header/Header';
import Button from '../Button/Button';
import { ListItemContext } from '../ListItemContext/ListItemContext';
import Item from '../Item/Item';
import Modal from '../Modal/Modal';



const Cart = () => {

  const {
    items,
    cart,
    onDelete,
    clearCart,
    openModal,
    closeModal,
    show,
    modal,
    addToFavorite,
    setCart,
    favorite,
} = useContext(ListItemContext)


  useEffect(() => {
      const newCart = cart.map(el => items.find(item => item.id === +el.id));
      newCart.map(el => el.idForDelete = Math.random());
      setCart(newCart)
  }, [favorite])

    return (
        <div> 
            <Header
                  itemCount={cart.length}
                  favCount={favorite.length}
                  actions = {
                    <>
                    <Button 
                    btnClassName="button"
                    className="button"
                    onClick={clearCart}
                    text="Очистить корзину"/>
                    </>
                  } />
                  <ul className="item-list">
                  {cart.length === 0 && (
        <h2 className='emptyTitle'>
          Cart is empty
        </h2> )}
           {cart.map(el => (
             < Item   
             classNameIcon={"favor-icon"}
             isFavorite={el.isFavorite}
             onClick={() => addToFavorite(el)}
             idForDelete={el.idForDelete}         
             modalId={el.id}
             key={el.idForDelete}
             imgRoute={el.imgRoute}
             name={el.name}
             price={el.price}   
             actions = {
               <>
               <Button 
               modalId={el.id}
               btnClassName="button apply"
               onClick={() => openModal(el.id)}
               className="button"
               text="Удалить из корзины"/>
               </>
             }
             />            
           ))}
         </ul>  
         {
         show && <Modal
         header="Подвердите удаление товара из корзины"
         onClick={closeModal}
         modalId={modal.id}  
           imgRoute={modal.imgRoute}
           name={modal.name}
           price={modal.price}
           actions = {
            <>
            <Button 
            modalId={modal.idForDelete} 
            btnClassName="button apply"
            onClick={() => onDelete(modal.idForDelete)}
            className="button"
            text="Удалить из корзины"
            />
            <Button 
            modalId={modal.idForDelete}
            btnClassName="button confirm"
            onClick={closeModal}
            className="button"
            text="Оформить заказ"
            />
            </>
          }
         />        
         }    
        </div>
    );
};

export default Cart;