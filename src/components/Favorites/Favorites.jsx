import React, {useContext } from 'react';
import Header from '../Header/Header';
import Button from '../Button/Button';
import { ListItemContext } from '../ListItemContext/ListItemContext';
import Item from '../Item/Item';
import Modal from '../Modal/Modal';
import { useNavigate } from "react-router-dom";



const Favorites = () => {

  const navigate = useNavigate();
  
  const {
    cart,
    clearCart,
    closeModal,
    addToCart,
    show,
    modal,
    addToFavorite,
    favorite,
} = useContext(ListItemContext)

const moveTo = (e) => {
  const route = "/cart";
   navigate(route)
   closeModal()
}

    return (
        <div> 
            <Header
                  itemCount={cart.length}
                  favCount={favorite.length}
                  actions = {
                    <>
                    <Button 
                    btnClassName="button"
                    className="button"
                    onClick={clearCart}
                    text="Очистить корзину"/>
                    </>
                  } />
                  <ul className="item-list">
                 {favorite.length === 0 && (
        <h2 className='emptyTitle'>
          Favorite list is empty
        </h2>)}
           {favorite.map(el => (
             < Item         
             modalId={el.id}
             classNameIcon={"favor-icon"}
             isFavorite={el.isFavorite}
             onClick={() => addToFavorite(el)}
             key={el.id}
             imgRoute={el.imgRoute}
             name={el.name}
             price={el.price}   
             actions = {
              <>
              <Button 
              modalId={el.id}
              btnClassName="button apply"
              onClick={() => addToCart(el.id)}
              className="button"
              text="Добавить в корзину"/>
              </>
            }
             />            
           ))}
         </ul>  
         {
         show && <Modal
         header="Подвердите удаление товара из корзины"
         onClick={closeModal}
         modalId={modal.id}  
           imgRoute={modal.imgRoute}
           name={modal.name}
           price={modal.price}
           actions = {
            <>
            <Button 
            btnClassName="button apply"
            onClick={moveTo}
            className="button"
            text="Перейти в корзину"
            />
            <Button 
            btnClassName="button confirm"
            onClick={closeModal}
            className="button"
            text="Продолжить покупки"
            />
            </>
          }
         />        
         }    
        </div>
    );
};

export default Favorites;