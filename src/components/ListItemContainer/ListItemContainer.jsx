import React, {useState, useEffect } from 'react';
import { ListItemContext } from '../ListItemContext/ListItemContext';


const ListItemContainer = (props) => {

    const [items, setItems] = useState([]);
    const [cart, setCart] = useState([]);
    const [show, showModal] = useState(false);
    const [modal, modalForm] = useState();
    const [favorite, setFavorite] = useState([]);  

    useEffect(() => {
      fetch('./data/json-data.json')
      .then(response => response.json())
      .then(item => {
        setItems(item);
      if (localStorage.getItem("cart") !== null) {setCart(JSON.parse(localStorage["cart"]))};
      })
  }, []);

    useEffect(() => {
      if (localStorage.getItem("favorites") !== null) {setFavorite(JSON.parse(localStorage["favorites"]))
    };
  }, [])


  const openModal = (id) => {
    showModal(true);
    const dataForModal = cart.find(item => item.id === +id); 
    modalForm(dataForModal);  
  }

           
  const closeModal = () => {
    showModal(false)  
  }

  
  const addToCart = (id) => {
    showModal(true);
    let dataForModal = items.find(item => item.id === +id); 
     modalForm(dataForModal = {...dataForModal, idForDelete: Date.now()});
    cart.push(dataForModal);
    localStorage.setItem('cart', JSON.stringify(cart));
  }
  

  const clearCart = () => {
      localStorage.removeItem('cart');
      setCart([]);
    }


  const handleDelete = (id) => {
    const newCart = cart.filter((el) => {
      if (el.idForDelete === id) {
        return false
      } else {
        return true
      }
    })
    setCart(newCart)
    localStorage.setItem('cart', JSON.stringify(newCart));
    closeModal();
  }
  

  useEffect(() => {
    const newItems = items.map((el) => {
      favorite.includes(el)
        ? (el.isFavorite = true)
        : (el.isFavorite = false);
      return el;
    });
    setItems(newItems);
  }, [favorite, cart])
  

  const addToFavorite = (el) => {
  if (favorite.includes(el)) {
      const newFavorites = favorite.filter((item) => item.id !== el.id);
      setFavorite(newFavorites);
    } else {
      setFavorite(arr => [...arr, el])
    };
  }

  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorite));
  }, [favorite])


  const { children } = props;


    return (
        <ListItemContext.Provider value={{
            items,
            cart: cart,
            setCart: setCart,
            clearCart: clearCart,
            onDelete: handleDelete,
            addToCart: addToCart,
            closeModal: closeModal,
            addToFavorite: addToFavorite,
            openModal: openModal,
            show,
            modal,
            favorite,
        }}>
        {children}
        </ListItemContext.Provider>
      );
    
};

export default ListItemContainer;
