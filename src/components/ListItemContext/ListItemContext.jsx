import React from "react";

export const ListItemContext = React.createContext({
  items: [],
  cart: [],
  setCart: () => {}, 
  clearCart: () => {},
  onDelete: () => {},
  addToCart: () => {},
  closeModal: () => {},
  addToFavorite: () => {},
  show: '',
  modal: '',
  openModal: () => {},
  favorite: [],
});
